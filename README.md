# aws-s3-private-access

Access files from private S3 bucket using a REST API and pre-signed URLS.

1. The client makes an HTTP GET request to API Gateway, and the Lambda function generates and returns a presigned S3 URL.
2. The client uploads the image to S3 directly, using the resigned S3 URL.

![image](https://theburningmonk.com/wp-content/uploads/2020/04/img_5e8ee37e797a2.png)

> Picture courtesy of https://theburningmonk.com/

## API usage

See the [Open API v3 specification](openapi.yml).

## Installation

Deploy the blueprint with




```sh
# Set your profile in environment
export AWS_PROFILE="profileName2" && export AWS_REGION=eu-west-1

npm ci
#sls deploy (note the space between -- ad deploy)
npm run sls -- deploy 
# or use a profile directly
npm run sls -- deploy --aws-profile devProfile
```

### Show created resources

```sh
npm run sls -- info

service Information
service: aws-s3-private-access
stage: dev
region: eu-west-1
stack: aws-s3-private-access-dev
resources: 39
api keys:
  aws-s3-private-access-dev: <EDITED>
endpoints:
  GET - https://<EDITED>.execute-api.eu-west-1.amazonaws.com/dev/apiStatus
  GET - https://<EDITED>.execute-api.eu-west-1.amazonaws.com/dev/getUploadURL/{filename}
  GET - https://<EDITED>.execute-api.eu-west-1.amazonaws.com/dev/getDownloadURL/{filename}
  GET - https://<EDITED>.execute-api.eu-west-1.amazonaws.com/dev/getFileList
functions:
  getApiStatus: aws-s3-private-access-dev-getApiStatus
  getUploadURL: aws-s3-private-access-dev-getUploadURL
  getDownloadURL: aws-s3-private-access-dev-getDownloadURL
  listFiles: aws-s3-private-access-dev-listFiles
layers:
  None
```

### run tests

```sh
# set API key (you can find it in the output of sls deploy or by running sls info)
export AWS_S3_PRIVATE_TOKEN="12345.."
# Launch E2E tests
npm test
```

### using custom domain name

Supports the use of custom domain name via serverless domain manager plugin (<https://www.npmjs.com/package/serverless-domain-manager>).

We set the target domain name with an environment variable.

```bash
# Set the domain name you want to use.
export SLS_BASE_DOMAIN="my-very-own-domain.com"
# Create related domain (cloud front distribution)
npm run sls -- create_domain
# Deploy with this domain
npm run sls -- deploy
```

When working with your own copy of this repository and/or always use the same domain, you may prefer to hard-code the `baseDomain` and/or `basePath` values in `serverless.yml` file.

```yml
custom:
  baseDomain: ${env:SLS_BASE_DOMAIN, "my-domain.com" }
  customDomain:
    domainName: api-${self:provider.stage}.${self:custom.baseDomain}
    basePath: myservice
```

### Update OAS V3 spec

```sh
npm run sls -- openapi generate
```

### Gitlab CI

This project comes with a `.gitlab-ci.yml` file that

- performs static checks (lint, npm package security check)
- deploys to Staging environment
- runs End To End integration tests

Required variables to set in your project settings

| variable name                 | Content                                                                      |
| ----------------------------- | ---------------------------------------------------------------------------- |
| AWS_ACCESS_KEY_ID_STAGING     | ID of AWS access key to use for staging environment                          |
| AWS_SECRET_ACCESS_KEY_STAGING | Secret key value for staging environment                                     |
| AWS_S3_PRIVATE_TOKEN_STAGING  | The API key for staging environment (used for integration testing)           |
| AWS_REGION                    | Region to deploy to (e.g. eu-west-1)                                         |
| SLS_BASE_DOMAIN               | Domain to use with _custom domain_ plugin (not used for staging environment) |

## Sample python client

### Usage

```sh
#Create a virtual env and activate it
cd sample_client
python3 -m venv venv
source venv/bin/activate
# Install dependencies (from inside venv)
pip install -r requirements.txt
# Provide API key
export API_KEY="123456"
# Run tests
python -m pytest tests
```

## Design

This project exposes a REST API that allows to list, upload or download files stored in a _private_ S3 bucket.

- Build with the serverless framework.
- The bucket is private. You can query the API to retrieve a temporary _pre-signed_ URLS to upload or download a file.
- API access is protected by an API Key + usage plan
- Generate OpenAPI specification from the serverless framework.

## References

- Yan Cui's original article: <https://theburningmonk.com/2020/04/hit-the-6mb-lambda-payload-limit-heres-what-you-can-do/>
- Zac Charles's article: with some code samples <https://medium.com/@zaccharles/s3-uploads-proxies-vs-presigned-urls-vs-presigned-posts-9661e2b37932>
