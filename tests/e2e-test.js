"use strict";

//Require the dev-dependencies
const { getApiGatewayUrl } = require('serverless-plugin-test-helper');
let chai = require("chai");
let chaiHttp = require("chai-http");
chai.use(chaiHttp);
const expect = require("chai").expect;
let fs = require("fs");

const API_BASE_URL = getApiGatewayUrl();
const VALID_API_KEY = process.env.AWS_S3_PRIVATE_TOKEN;

const TEST_FILE_NAME = "dummy_file.txt"
const TESTED_FILE = "./tests/dummy_file.txt";




describe("Get API status", () => {

  it("it should return a ok status, even without API key", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/apiStatus")
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;
        expect(res.body).to.have.property("status");
        expect(res.body.status).equals("ok");
        done();
      });
  });
});


describe("Request a pre-signed upload url", () => {
  it("it should return an upload URL", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/getUploadURL/aFileName.abc")
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;

        expect(res.body).to.have.property("uploadURL");

        done();
      });
  }).timeout(10000);
});

describe("Request a pre-signed download url", () => {
  it("it should fail 403 when the filename parameter is missing in the request", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/getDownloadURL")
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(403);

        done();
      });
  }).timeout(10000);

  it("it should return a pre-signed URL even if we target request non-existing file name", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/getDownloadURL/this-thing-cannot-exist.zip")
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;
        expect(res.body).to.have.property("downloadURL");

        done();
      });
  });

  it("it should return a download URL if I provide an valid key (8d340a21-3b57-452c-96bb-8679ed2539e9.zip)", (done) => {
    let key = "8d340a21-3b57-452c-96bb-8679ed2539e9.zip";
    chai
      .request(API_BASE_URL)
      .get("/getDownloadURL/" + key)
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;

        expect(res.body).to.have.property("downloadURL");
        //console.log("dl url for pre-existing key", res.body.downloadURL);
        done();
      });
  });

  it("it should return a download URL if I provide a target valid filename (123456.zip)", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/getDownloadURL/123456.zip")
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;

        expect(res.body).to.have.property("downloadURL");

        done();
      });
  });
});

describe("Download an non existing file should return 404", () => {
  it("it should return a 404 when downloading non-existing file", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/getDownloadURL/this-thing-cannot-exist.zip")
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;
        expect(res.body).to.have.property("downloadURL");

        let downloadUrl = res.body.downloadURL;

        // Now download the file
        chai
          .request(downloadUrl)
          .get("")
          .end((err, res) => {
            expect(res.statusCode).to.equal(404);
            done();
          });
      });
  });
});

describe("End to End test (upload and download)", () => {
  it("it should upload and download fine", (done) => {
    // Get an upload url
    chai
      .request(API_BASE_URL)
      .get("/getUploadURL/"+TEST_FILE_NAME)
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.body).to.have.property("uploadURL");

        let uploadUrl = res.body.uploadURL;

        // Now Upload
        chai
          .request(uploadUrl)
          .put("")
          .send(fs.readFileSync(TESTED_FILE))
          .set("X-API-Key", VALID_API_KEY)
          .end((err, res) => {
            expect(res.statusCode).to.equal(200);

            // Get at download URL
            chai
              .request(API_BASE_URL)
              .get("/getDownloadURL/" + TEST_FILE_NAME)
              .set("X-API-Key", VALID_API_KEY)
              .end((err, res) => {
                expect(res.statusCode).to.equal(200);
                expect(res.body).to.have.property("downloadURL");

                let downloadUrl = res.body.downloadURL;
                //console.log('downloadurl:'+downloadUrl);

                // Now download the file
                chai
                  .request(downloadUrl)
                  .get("")                  
                  .end((err, res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res).to.have.header("content-length");
                    expect(res).to.have.header(
                      "content-length",
                      fs.statSync(TESTED_FILE).size.toString()
                    );
                    expect(res).to.have.header(
                      "content-type",
                      "binary/octet-stream"
                      );
                      done();
                  });
              });
          });
      });
  });
});

describe("List files", () => {
  it("it should return a list containing the test file name", (done) => {
    chai
      .request(API_BASE_URL)
      .get("/getFileList")
      .set("X-API-Key", VALID_API_KEY)
      .end((err, res) => {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.not.be.null;
        expect(res).to.be.json;

        expect(res.body).to.have.property("files");
     
        expect(res.body.files).to.have.property("Contents")
        expect(res.body.files.Contents).length > 0
        expect(res.body.files.Contents[0]).to.be.an('object').that.has.all.keys('ChecksumAlgorithm','ETag', 'Key', 'LastModified', 'Size', 'StorageClass');
        expect(res.body.files.Contents[0].Key).to.equal(TEST_FILE_NAME)

        done();
      });
  }).timeout(10000);
});



