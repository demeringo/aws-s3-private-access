""" Interacts with s3 private service """
# pylint: disable=W1203
import os
import logging
import wget
import requests

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

API_BASE_URL = "https://pz6pa9i430.execute-api.eu-west-1.amazonaws.com/staging"
API_KEY = os.environ['API_KEY']
AUTH_HEADERS = {"X-Api-Key": API_KEY}
TIMEOUT = 10


def get_api_status():
    """ Returns API status (ok or ko)"""
    url = API_BASE_URL+"/apiStatus"
    logger.debug(f'Requesting API status to: {url}')
    response = requests.get(url, timeout=TIMEOUT)
    response.raise_for_status()
    status = response.json()["status"]
    return status


def get_temporary_upload_url(filepath):
    """ Returns a temporary pre-signed url to use for upload"""
    filename_without_path = os.path.basename(filepath)
    url = API_BASE_URL+"/getUploadURL/" + filename_without_path
    logger.debug(f'Requesting an upload URL to: {url}')
    response = requests.get(url, headers=AUTH_HEADERS, timeout=TIMEOUT)
    response.raise_for_status()
    temp_url = response.json()["uploadURL"]
    return temp_url


def upload_file_to_url(file, url):
    """ Uploads file to url """
    logger.debug(f'Uploading {file} to {url}')
    with open(file, 'rb') as my_file:
        files = {'file': (file, my_file)}
        response = requests.put(url, files=files, timeout=TIMEOUT)
    logging.info(f'File uploaded with response: {response}')


def generate_file_key_from_full_name(local_file_name):
    """ Returns a file key (usually the file basename) from the full path
    """
    return os.path.basename(local_file_name)


def upload(local_file_name):
    """ uploads the local file to the storage service """
    logger.debug(f'Uploading {local_file_name}')

    file_key = generate_file_key_from_full_name(local_file_name)
    tmp_upload_url = get_temporary_upload_url(local_file_name)
    upload_file_to_url(local_file_name, tmp_upload_url)

    logging.info(f'File uploaded. (File key is {file_key})')
    return file_key


def get_temporary_download_url(key):
    """ Returns a temporary download URL for the s3 key (e.g. the file name like 123.ext) """
    url = API_BASE_URL+"/getDownloadURL/"+key
    logger.debug(f'Requesting download URL to: {url}')
    response = requests.get(url, headers=AUTH_HEADERS, timeout=TIMEOUT)
    response.raise_for_status()
    return response.json()["downloadURL"]


def download_url(url, local_file_name):
    """ Download an url and save it as local_file_name"""
    logger.debug(f'Downloading {url} as {local_file_name}')
    wget.download(url, local_file_name)


def download(key, destination_file_name):
    """ Download the file whose key is passed in argument ans save it as destination_file_name"""
    logger.debug(f'Downloading {key} as {destination_file_name}')
    tmp_download_url = get_temporary_download_url(key)
    download_url(tmp_download_url, destination_file_name)


def get_file_list():
    """ Retrieve the list of files in the bucket"""
    url = API_BASE_URL+"/getFileList"
    logger.debug(f'Requesting file list to: {url}')
    response = requests.get(url, headers=AUTH_HEADERS, timeout=TIMEOUT)
    response.raise_for_status()
    return response.text
