import os
import json
import shutil
import unittest
import tempfile

from assertpy import assert_that
import s3_private_service as api

TEST_FILE_NAME = 'dummyfile.txt'
TEST_FILE_FULL_PATH = os.path.join('tests/test-data', TEST_FILE_NAME)

FILE_THAT_DOES_NOT_EXIST = os.path.join('tests/test-data', 'a-missing-file')


class TestS3PrivateService(unittest.TestCase):
    def setUp(self):
        self.LOCAL_TMP_DIR = tempfile.mkdtemp()
        pass

    def tearDown(self):
        shutil.rmtree(self.LOCAL_TMP_DIR)
        pass

    def test_api_status(self):
        status = api.get_api_status()
        assert_that(status).starts_with("ok")

    def test_generate_file_key_from_full_path(self):
        key = api.generate_file_key_from_full_name(TEST_FILE_FULL_PATH)
        assert_that(key).is_equal_to(TEST_FILE_NAME)

    def test_generate_file_key_from_file_name(self):
        key = api.generate_file_key_from_full_name(TEST_FILE_NAME)
        assert_that(key).is_equal_to(TEST_FILE_NAME)

    def test_upload_existing_file(self):
        url = api.get_temporary_upload_url(TEST_FILE_NAME)
        api.upload_file_to_url(TEST_FILE_FULL_PATH, url)

    def test_get_temporary_download_url(self):
        url = api.get_temporary_download_url(TEST_FILE_NAME)
        assert_that(url).is_not_empty()
        assert_that(url).starts_with(
            "https://aws-s3-private-access-staging-mystoragebucket")

    def test_download_url_that_does_not_exist(self):
        with self.assertRaises(Exception):
            api.download_url("invalid URL", "whatever")

    def test_download_url_valid_file(self):
        local_file_name = os.path.join(self.LOCAL_TMP_DIR, "downloaded.txt")

        url = api.get_temporary_download_url(TEST_FILE_NAME)

        api.download_url(url, local_file_name)
        assert_that(local_file_name).exists()
        assert_that(local_file_name).is_file()

    def test_list_files(self):
        listed = api.get_file_list()

        res = json.loads(listed)['files']
        assert_that(res).contains_key('Contents')
        assert_that(res['Contents']).is_not_empty()
        assert_that(res['Contents'][0]).contains_key('Key')
        assert_that(res['Contents'][0]).contains_key('LastModified')

    def test_atomic_upload_with_valid_file(self):
        new_file_key = api.upload(TEST_FILE_FULL_PATH)
        assert_that(new_file_key).is_equal_to(
            api.generate_file_key_from_full_name(TEST_FILE_FULL_PATH))

    def test_atomic_upload_with_wrong_file(self):
        with self.assertRaises(Exception):
            api.upload(FILE_THAT_DOES_NOT_EXIST)

    def test_atomic_download(self):
        remote_file_key = TEST_FILE_NAME
        local_file_name = os.path.join(self.LOCAL_TMP_DIR, "retrieved.txt")

        api.download(remote_file_key, local_file_name)
        assert_that(local_file_name).exists()
        assert_that(local_file_name).is_file()


if __name__ == '__main__':
    unittest.main()
