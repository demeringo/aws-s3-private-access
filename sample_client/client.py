""" Sample client code for the AWS-S3-PRIVATE-ACCESS API """
import logging
from . import s3_private_service as api

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)


def main():
    """ Demo call """

    file_full_name = 'tests/test-data/dummyfile.txt'

    status = api.get_api_status()
    logger.info('API status: %s', status)

    # Upload a file (and save it's new key)
    key_of_the_stored_file = api.upload(file_full_name)
    logger.info('Uploaded %s, now stored as %s',
                file_full_name, key_of_the_stored_file)

    # List content
    storage_content = api.get_file_list()
    logger.info('List storage content: %s', storage_content)

    # Download a file to temp directory
    api.download(key_of_the_stored_file, '/tmp/retrieved_file.txt')
    logger.info('Downloaded %s and saved as %s',
                key_of_the_stored_file, '/tmp/retrieved_file.txt')


if __name__ == "__main__":
    main()
