"use strict";

const BUCKET = process.env.StorageBucket;
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.AWS_REGION || "eu-west-1" });
var s3 = new AWS.S3({ signatureVersion: "v4" });

// eslint-disable-next-line no-unused-vars
module.exports.getDownloadURL = async (event, context) => {
  let key = event.pathParameters.filename;

  let params = {
    Bucket: BUCKET,
    Key: `${key}`,
    Expires: 300,
  };
  console.log("getDownloadURL : ", params);
  
  let url = await s3.getSignedUrl("getObject", params);

  const response = {
    statusCode: 200,
    body: JSON.stringify({ downloadURL: url }),
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  };
  console.log("Result: ", response);
  return response;
};
