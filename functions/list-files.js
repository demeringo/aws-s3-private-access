"use strict";

const BUCKET = process.env.StorageBucket;
const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.AWS_REGION || "eu-west-1" });
var s3 = new AWS.S3({ signatureVersion: "v4" });

// eslint-disable-next-line no-unused-vars
module.exports.handler = async (event, context) => {

  var params = {
    Bucket: BUCKET
  };
  const data = await s3.listObjectsV2(params).promise();
  
  const response = {
    statusCode: 200,
    body: JSON.stringify({ files: data }),
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  };
  console.log("Result: ", response);
  return response;
};
