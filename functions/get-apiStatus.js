"use strict";

module.exports.status = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      status: `ok`,
    }),
  };

  callback(null, response);
};
